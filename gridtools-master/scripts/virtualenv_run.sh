#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
# echo DIR is ${DIR}
# which virtualenvwrapper.sh
. virtualenvwrapper.sh
workon ${1}
which python
python ${DIR}/../gridtools/virtualenv_run.py ${2}
