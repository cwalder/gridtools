import sys, os

import gridtools.helpers as gh
import gridtools.paths as paths

_infile = '%s/input.pickle'
_outfile = '%s/output.pickle'


class wrap(object):

    def __init__(self, virtualenv=gh.virtualenv, basepath='/tmp/'):
        self.virtualenv = virtualenv
        self.path = paths.output('virtualenv_run', basepath=basepath)
        print(self.path)

    def __call__(self, f):
        def g(*args, **kwargs):
            gh.dump((f, (args, kwargs)), filename=_infile % self.path)
            code = os.system('%s/virtualenv_run.sh %s %s' % (paths._script_path, self.virtualenv, self.path))
            if code != 0:
                raise Exception('os.system returned %i' % code)
            rval = gh.load(_outfile % self.path)
            return rval

        return g


if __name__ == '__main__':

    try:
        print('cleanup with:')
        print("ps aux | grep virtualenv | cut -c 17-21 | xargs kill -9")
        print(sys.argv)
        path = sys.argv[1]
        f, (args, kwargs) = gh.load(_infile % path)
        rval = f(*args, **kwargs)
        gh.dump(rval, _outfile % path)
    except Exception as e:
        import traceback

        traceback.print_exc()
        sys.exit(1)
