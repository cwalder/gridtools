'''
- you need to use virtualenv, it will run remotely in the virtualenv of the same name as that which executes this script
- for "--local False" (to run on the cluster) you need to be on the sge submission host 
- if jobs fail you can rerun them from within the job directory this script creates

e.g.

python qsub.py --local True ../experiments/template.py --y 10
'''

import sys, os, subprocess, time
from gridtools import helpers, paths


sge_hosts = {
    'cluster.it.nicta.com.au'
}
slurm_hosts = {
    'bragg-gpu.hpc.csiro.au',
    'bracewell.hpc.csiro.au'
}
host2submissiondir = {
    'cluster.it.nicta.com.au': '/opt/gridengine/bin/linux-x64/',
    'bragg-gpu.hpc.csiro.au': '/cm/shared/apps/slurm/15.08.6/bin/',
    'bracewell.hpc.csiro.au': '/cm/shared/apps/slurm/current/bin/'
}


def host_type(s):
    if s in sge_hosts:
        return 'sge'
    else:
        return 'slurm'


def execute_background(command, outfn, errfn):
    subprocess.Popen(
        ['nohup', command],
        stdout=open(outfn, 'w'),
        stderr=open(errfn, 'a'),
        preexec_fn=os.setpgrp
    )


@helpers.dumpArgs(abbreviation_nchars=1024)
def raw_main(
        virtualenv = helpers.virtualenv,
        slots = 1,
        gpus = 1,
        mem = 12 * 1024,
        maxtime='72:0:0',
        basepath=None,
        python_script=None,
        arguments='',
        qsub_args='',
        local = False,
        submit_host = None,
        rsync_code = True,
        dump_git_info = True,
        background_submit = False,
):

    path = paths.output('qsub/', basepath=basepath)
    helpers.dumpArgsToDirectory(path, git_info=dump_git_info)

    if python_script.endswith('.pyc'):
        python_script = python_script[:-1]

    python_script_directory = os.path.dirname(python_script)

    tee = helpers.Tee('%s/output.txt' % path, compress=False)

    wrapper_fn = '%s/run.sh' % path
    with open(wrapper_fn, 'w') as f:
        f.write(('#!/bin/bash\n' + 
                 'env | grep SHELL\n' + 
                 'env | grep JOB_ID\n' +
                 'module list\n' +
                 'source $(which virtualenvwrapper_lazy.sh)\n' +
                 'workon {virtualenv}\n' + 
                 'echo VIRTUAL_ENV is $VIRTUAL_ENV\n' + 
                 'export CLUSTER_JOB_HOME={path}\n' +
                 'cd {python_script_directory}\n' +
                 'python {python_script} {arguments}\n' ).format(**locals())
        )
    os.system('chmod a+x %s' % wrapper_fn)

    u_path, u_wrapper_fn = list(map(paths.unexpand_home, (path, wrapper_fn)))
    u_outfn = '%s/out.txt' % u_path
    u_errfn = '%s/err.txt' % u_path

    mem = int(mem)

    if host_type(submit_host) == 'sge':
        qsub_args += ' -S /bin/bash -o {u_outfn} -e {u_errfn}'.format(**locals())
        if slots is not None and slots > 1:
            cmd = 'qsub -pe orte-slots {slots} {qsub_args} {u_wrapper_fn}'.format(**locals())
        else:
            cmd = 'qsub -p 0 {qsub_args} {u_wrapper_fn}'.format(**locals())
        if not local and submit_host is not None:
            cmd = 'ssh %s "source /etc/bashrc; %s/%s"' % (submit_host, host2submissiondir[submit_host], cmd)
    else:
        qsub_args += ' -o {u_outfn} '.format(**locals())
        # slurm automatically combines stdour and stderr:
        # qsub_args += ' -e {u_errfn} '.format(**locals())
        if slots is not None and slots > 1:
            cmd = 'sbatch --time {maxtime} --mem {mem} --nodes 1 --tasks-per-node 1 --cpus-per-task {slots} --gres gpu:{gpus} {qsub_args} {u_wrapper_fn}'.format(**locals())
        else:
            cmd = 'sbatch --time {maxtime} --mem {mem} --nodes 1 --tasks-per-node 1 --gres gpu:{gpus} {qsub_args} {u_wrapper_fn}'.format(**locals())
        if not local and submit_host is not None and not helpers.on_host(submit_host):
            cmd = 'ssh %s "%s/%s"' % (submit_host, host2submissiondir[submit_host], cmd)

    submit_fn = '%s/submit.sh' % path
    with open(submit_fn, 'w') as f:
        f.write('#!/bin/bash\n{cmd}\n'.format(**locals()))
    os.system('chmod a+x %s' % submit_fn)

    if local:
        print('local command:')
        print(u_wrapper_fn)
        execute_background(*list(map(os.path.expanduser, (u_wrapper_fn, u_outfn, u_errfn))))
    else:
        if submit_host is not None:
            helpers.rsync(path, submit_host, upload=True)
            helpers.rsync_roots('sync_roots', submit_host)
            if rsync_code:
                helpers.rsync_roots('code_roots', submit_host)
        if background_submit:
            cmd += ' &'
        print('qsub command:')
        print(cmd)
        time.sleep(0.1)
        os.system(cmd)
        if background_submit:
            time.sleep(0.5)

    print(paths.unexpand_home(path))

    tee.to_file()

    return path


main = helpers.auto_parser_inspector(allow_missing=True)(raw_main)


if __name__ == '__main__':

    i = [j for j, a in enumerate(sys.argv) if a[-3:] == '.py' and j > 0]
    assert len(i) == 1, ('for help go python qsub.py --help file.py', sys.argv)
    i = i[0]
    python_script = paths.unexpand_home(os.path.abspath(sys.argv[i]))
    arguments = ' '.join(sys.argv[(i+1):])
    sys.argv = sys.argv[:i]
    main(python_script=python_script, arguments=arguments)
