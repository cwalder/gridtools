import glob, functools, time, os, random, traceback, collections, copy

from gridtools import paths, qsub, helpers

_jobs_path = '%s/jobs.pkl'
_commonind2arg_pickle_path = '%s/commonind2arg.pkl'
_arg_path = '%s/arg_%.7i.pkl'
_uniqueind2arg_pickle_path = '%s/uniqueind2arg_%.7i.pkl'
_index_rval_path = '%s/index_rval.pkl'


class JobsNotFinishedException(Exception):
    pass


@helpers.memodict
def cached_load(filename):
    return helpers.load(filename)


class Jobs(object):

    def __init__(self, slots=1, gpus=1, local=True, basepath='~/output/', submit_host='cluster.it.nicta.com.au', qsub_kwargs=dict(), yield_partial=False):

        self.slots = slots
        self.gpus = gpus
        self.local = local
        self.job_path = paths.unexpand_home(paths.output('jobs', basepath=basepath))
        self.submit_host = submit_host
        self.qsub_kwargs = qsub_kwargs
        self.yield_partial = yield_partial

        self.fns = []
        self.argsinds = []
        self.kwargsinds = []
        self.id2ind = dict()
        self.ind2arg = dict()
        self.short_circuit_rvals = []
        self.qsub_kwargs_list = []

    def _add_arg(self, arg):
        if id(arg) in self.id2ind:
            return self.id2ind[id(arg)]
        else:
            newind = len(self.id2ind)
            self.id2ind[id(arg)] = newind
            self.ind2arg[newind] = arg
            return newind

    def add(self, fn, *args, **kwargs):
        if self.local == 'short_circuit':
            self.short_circuit_rvals.append(copy.deepcopy(fn(*args, **kwargs)))
        else:
            self.fns.append(self._add_arg(fn))
            self.argsinds.append(tuple(map(self._add_arg, args)))
            self.kwargsinds.append({k: self._add_arg(v) for k, v in kwargs.items()})
            self.qsub_kwargs_list.append(copy.deepcopy(self.qsub_kwargs))

    def __len__(self):
        return len(self.fns)

    def qsub(self):

        helpers.dump(self, _jobs_path % self.job_path)

        if self.local == 'short_circuit':
            return

        indsbyjob = []
        for index, (fn, argsind, kwargsind) in enumerate(zip(self.fns, self.argsinds, self.kwargsinds)):
            indsbyjob.append([fn]+list(argsind)+list(kwargsind.values()))
        commoninds = functools.reduce(set.intersection, map(set, indsbyjob))
        uniqueinds = [set(_).difference(commoninds) for _ in indsbyjob]
        commonind2arg = {k: self.ind2arg[k] for k in commoninds}
        helpers.dump(commonind2arg, _commonind2arg_pickle_path % self.job_path)

        for index, (fn, argsind, kwargsind, uniqueind, qsub_kwargs) in enumerate(zip(self.fns, self.argsinds, self.kwargsinds, uniqueinds, self.qsub_kwargs_list)):
            print('submitted %i / %i' % (index, len(self.fns)))
            arg = (fn, argsind, kwargsind)
            helpers.dump(arg, _arg_path % (self.job_path, index))
            uniqueind2arg = {k: self.ind2arg[k] for k in uniqueind}
            helpers.dump(uniqueind2arg, _uniqueind2arg_pickle_path % (self.job_path, index))
            if not self.local:
                helpers.rsync(self.job_path, self.submit_host, upload=True)
            arguments = "--job_path \"'%s'\" --index %i --yield_partial %s" % (paths.unexpand_home(self.job_path), index, self.yield_partial)
            kw = dict(
                python_script=paths.unexpand_home(__file__),
                arguments=arguments,
                slots=self.slots,
                gpus=self.gpus,
                local=self.local,
                basepath=paths.unexpand_home(self.job_path),
                submit_host=self.submit_host,
                rsync_code=(index==0),
                dump_git_info=(index==0),
            )
            kw.update(qsub_kwargs)
            qsub.raw_main(**kw)

    def qsub_paths(self, indices=None):
        # hack
        if indices is None:
            indices = range(len(self))
        pp = glob.glob(os.path.expanduser('%s/qsub/*/dumpArgs/argdict.pickle' % self.job_path))
        argdicts = {p: helpers.load(p) for p in pp}
        index2path = {int(argdict['arguments'].split('--index')[-1].split()[0].strip()): paths.unexpand_home(path) for path, argdict in argdicts.items()}
        return ['%s/' % os.path.dirname(os.path.dirname(index2path[i])) for i in indices]

    def collect(self, block=False, as_dict=False, verbose=False, force_rsync=False, force_no_rsync=False, max_load=None, error_fmt=None):

        if error_fmt is None:
            error_fmt = Jobs.error_fmt('submit path')

        if self.local == 'short_circuit':
            if as_dict:
                return dict(enumerate(self.short_circuit_rvals))
            else:
                return tuple(self.short_circuit_rvals)

        if block:

            while True:
                try:
                    return self.collect(block=False, as_dict=as_dict, verbose=verbose)
                except JobsNotFinishedException:
                    if verbose:                        
                        traceback.print_exc()
                    time.sleep(1 if self.local else 30)

        else:

            paths = glob.glob(os.path.expanduser(_index_rval_path % ('%s/cluster/*' % self.job_path)))
            
            if ((force_rsync or len(paths) < len(self)) and not self.local) and not force_no_rsync:
                helpers.rsync(self.job_path, self.submit_host, upload=False, verbose=False)
                paths = glob.glob(os.path.expanduser(_index_rval_path % ('%s/cluster/*' % self.job_path)))

            results_dict = dict()
            bad_paths = []
            for path in paths:
                if max_load is not None and (len(results_dict) > max_load):
                    break
                try:
                    results_dict[path] = cached_load(path)
                except:
                    bad_paths.append(path)
                    traceback.print_exc()

            if as_dict:
                return dict(results_dict.values())
            else:
                if len(results_dict) == 0:
                    indices, rvals = tuple(), tuple()
                else:
                    indices, rvals = zip(*sorted(results_dict.values(), key=lambda x: x[0]))
                if len(indices) != len(set(indices)):
                    raise JobsNotFinishedException('duplicate results: %s\nbad_paths: %s'% (str(collections.Counter(indices)), str(bad_paths)))
                if indices != tuple(range(len(self))):
                    helpers.rsync(self.job_path, self.submit_host, upload=False, verbose=False)
                    missing = sorted(set(range(len(self))).difference(set(indices)))
                    missing_qsub_paths = self.qsub_paths(missing)
                    missing_qsub_paths_submit_scripts = ['%s/submit.sh' % _ for _ in missing_qsub_paths]
                    missing_qsub_paths_actions = [error_fmt % _ for _ in missing_qsub_paths]
                    raise JobsNotFinishedException('%i / %i unfinished jobs: %s\nresult paths which failed to load:\n%s\nsubmit scripts for missing jobs:\n%s\naction for missing jobs:\n%s' % (len(self)-len(indices), len(self), str(missing), '\n'.join(bad_paths), '\n'.join(missing_qsub_paths_submit_scripts), '\n'.join(missing_qsub_paths_actions)))
                return rvals
    
    @classmethod
    def preset(cls, s):
        # sed -i "s/mem 12288/mem 32000/" qsub/*/submit.sh
        if s == 'local':
            return Jobs(local=True)
        elif s == 'short_circuit':
            return Jobs(local='short_circuit')
        elif s == 'bragg':
            return Jobs(slots=2, gpus=1, local=False, submit_host='bragg-gpu.hpc.csiro.au')
        elif s == 'bragg_32G':
            return Jobs(slots=2, gpus=1, local=False, submit_host='bragg-gpu.hpc.csiro.au', qsub_kwargs=dict(mem=32 * 1024))
        elif s == 'bragg_64G':
            return Jobs(slots=2, gpus=1, local=False, submit_host='bragg-gpu.hpc.csiro.au', qsub_kwargs=dict(mem=62 * 1024))
        elif s == 'bragg_120G':
            return Jobs(slots=2, gpus=1, local=False, submit_host='bragg-gpu.hpc.csiro.au', qsub_kwargs=dict(mem=120 * 1024))
        elif s == 'braggcpu':
            return Jobs(slots=1, gpus=0, local=False, submit_host='bragg-gpu.hpc.csiro.au')
        elif s == 'braggcpu_4G':
            return Jobs(slots=1, gpus=0, local=False, submit_host='bragg-gpu.hpc.csiro.au', qsub_kwargs=dict(mem=4 * 1024))
        elif s == 'bracewell_48G':
            return Jobs(slots=4, gpus=1, local=False, submit_host='bracewell.hpc.csiro.au', qsub_kwargs=dict(mem=48 * 1024))
        else:
            raise Exception('bad preset %s' % s)

    @classmethod
    def error_fmt(cls, s, *args):
        if s in ('time', 'mem'):
            return 'sed -i "s/%s %s/%s %s/" %%s/submit.sh' % (s, args[0], s, args[1])
        elif s == 'submit path':
            return '%s/submit.sh'

@helpers.dumpArgs()
def raw_main(job_path, index, yield_partial):

    random.seed(index)
    path = paths.output('cluster', basepath=job_path)
    helpers.dumpArgsToDirectory(path)

    tee = helpers.Tee('%s/output.txt' % path)

    ind2arg = helpers.load(_commonind2arg_pickle_path % job_path)
    uniqueind2arg = helpers.load(_uniqueind2arg_pickle_path % (job_path, index))
    ind2arg.update(uniqueind2arg)
    arg = helpers.load(_arg_path % (job_path, index))
    fn, args, kwargs = arg

    fn = ind2arg[fn]
    args = [ind2arg[_] for _ in args]
    kwargs = {k: ind2arg[v] for k, v in kwargs.items()}

    if yield_partial:
        for irval, rval in enumerate(fn(*args, **kwargs)):
            print('yielding partial result', irval)
            if irval > 0:
                os.system('mv %s %s.prev' % (_index_rval_path % path, _index_rval_path % path))
            print(helpers.dump((index, rval), _index_rval_path % path))
        if irval > 0:
            os.system('/bin/rm -f%s.prev' % (_index_rval_path % path))
    else:
        rval = fn(*args, **kwargs)
        print(helpers.dump((index, rval), _index_rval_path % path))

    tee.to_file()


main = helpers.auto_parser_inspector(allow_missing=False)(raw_main)


if __name__ == '__main__':

    main()
