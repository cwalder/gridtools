import shelve, dbm.ndbm, dbm.dumb, lockfile, time, traceback
import os.path
import numpy as np
try:
    import macpath
except:
    pass

import gridtools.helpers as gh

_global_memoize_recompute = False


def shelve_open(filename, flag="c"):
    # return shelve.Shelf(dbm.ndbm.open(filename, flag))
    return shelve.Shelf(dbm.dumb.open(filename, flag))
# shelve_open = shelve.open


def time_seeded_sleep(seconds):
    state = np.random.get_state()
    np.random.seed(int((time.time()%1) * np.iinfo(np.uint32).max))
    tsleep = np.random.rand() * seconds
    np.random.set_state(state)
    time.sleep(tsleep)


def expanduser(p):
    if p is None:
        return p
    else:
        try:
            p = macpath.expanduser(p)
        except:
            pass
        p = os.path.expanduser(p)
        return p


class Memoize(object):
    def __init__(self, filename=None, check=None, key_fn=lambda args, kwargs: str(tuple((args, tuple(sorted(kwargs.items()))))), verbose=False, recompute=False, interpreter_specific=True, none_on_error=False, none_on_stored_error=False):

        if check is None:
            check = lambda fval, *args, **kwargs: True

        self.check = check
        self.filename = expanduser(filename)
        if self.filename is not None:
            if interpreter_specific:
                self.filename += gh.virtualenv
            self.lockfile = lockfile.LockFile(self.filename)
            self.cache = None
        else:
            self.cache = dict()
        self.key_fn = key_fn
        self.evals = 0
        self.calls = 0
        self.verbose = verbose
        self.recompute = recompute
        self.f = None
        self.none_on_error = none_on_error
        self.none_on_stored_error = none_on_stored_error

    def getlock(self):
        if self.verbose:
            print('acquiring lock for', self.filename)
        while True:
            try:
                self.lockfile.acquire(timeout=10)
            except lockfile.AlreadyLocked:
                print('waiting for locked file', self.filename)
                time.sleep(1)
            except lockfile.LockFailed:
                raise
            else:
                break
        if self.verbose:
            print('acquired')

    def releaselock(self):
        if self.verbose:
            print('releasing lock for', self.filename)
        while True:
            try:
                self.lockfile.release()
            except lockfile.NotMyLock:
                print('waiting to unlock', self.filename)
                time.sleep(1)
            except lockfile.NotLocked:
                print('no need to unlock', self.filename)
                break
            else:
                break
        if self.verbose:
            print('released')

    def __call__(self, f):

        self.f = f

        def g(*args, **kwargs):
            self.calls += 1
            key = self.key_fn(args, kwargs)
            if self.recompute or _global_memoize_recompute:
                print('memoize: recomputing')
            if not (self.recompute or _global_memoize_recompute):
                new_cache = False
                if self.filename is not None:
                    try:
                        cache = shelve_open(self.filename, 'r')
                    except Exception as e:
                        if (2, 'No such file or directory') == e.args:
                            time_seeded_sleep(0.1)
                            self.getlock()
                            cache = shelve_open(self.filename, 'c')
                            cache.close()
                            self.releaselock()
                            new_cache = True
                        else:
                            traceback.print_exc()
                            raise
                else:
                    cache = self.cache
                if not new_cache and (key in cache):
                    if self.verbose:
                        print('hit')
                    rval = cache[key]
                    if self.filename is not None:
                        cache.close()
                    if isinstance(rval, Exception):
                        if self.none_on_stored_error:
                            return None
                        else:
                            if self.verbose:
                                print('not none_on_stored_error for error raising key %s' % key)
                    else:
                        return rval
                else:
                    if self.verbose:
                        print('missing key %s' % key)
            self.evals += 1
            checked = False
            while not checked:
                try:
                    fval = f(*args, **kwargs)
                except Exception as error:
                    if self.none_on_error:
                        print('memoize caught:')
                        traceback.print_exc()
                        fval = None
                        break
                    else:
                        fval = error
                        break
                checked = self.check(fval, *args, **kwargs)
            if self.filename is not None:
                self.getlock()
                cache = shelve_open(self.filename, 'c')
            else:
                cache = self.cache
            if self.verbose:
                print('adding key %s' % (key))
                # print('adding key %s value %s' % (key, fval))
            cache[key] = fval
            if self.filename is not None:
                cache.sync()
                cache.close()
                self.releaselock()
            if isinstance(fval, Exception):
                raise fval
            else:
                return fval

        if self.verbose:
            print('memoized', f)
        return g

    def manually_cache(self, fval, *args, **kwargs):
        key = self.key_fn(args, kwargs)
        if self.filename is not None:
            self.getlock()
            cache = shelve_open(self.filename)
        else:
            cache = self.cache
        if key not in cache:
            if self.verbose:
                print('inserting key %s' % key)
            cache[key] = fval
        if self.filename is not None:
            cache.sync()
            cache.close()
            self.releaselock()

    def check_args(self, args_kwargs, flag='c'):
        if self.filename is not None:
            cache = shelve_open(self.filename, flag)
        else:
            cache = self.cache
        not_found = []
        for i, (arg, kwarg) in enumerate(args_kwargs):
            key = self.key_fn(arg, kwarg)
            if key not in cache:
                not_found.append(i)
        if self.filename is not None:
            cache.close()
        return not_found

    def print_memoize_stats(self):
        print('memoized', self.f, 'calls =', self.calls, 'evals =', self.evals, 'eval percent =', (np.float64(self.evals) / np.float64(self.calls) * 100.0), 'cached =', (self.filename if self.cache is None else len(self.cache)))


