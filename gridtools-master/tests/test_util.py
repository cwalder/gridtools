# -*- coding: utf-8 -*-

import unittest
import random

import gridtools.helpers as gh
import gridtools.memoize as gm
from gridtools import virtualenv_run


class TestUtil(unittest.TestCase):

    def test_virtualenv_run(self):

        f = gh.pretty_duration
        g = virtualenv_run.wrap()(f)
        x = 60 * 30
        fx = f(x)
        gx = g(x)
        assert fx == gx

    def test_sub_list_and_sub_list_complement(self):

        assert gh.sub_list([0, 10, 20, 30], slice(1, None)) == [10, 20, 30]
        assert gh.sub_list([0, 10, 20, 30], [1, 2, 3]) == [10, 20, 30]
        assert gh.sub_list_complement([0, 10, 20, 30], slice(1, None)) == [0]
        assert gh.sub_list_complement([0, 10, 20, 30], [1, 2, 3]) == [0]

    def test_memoize(self):

        def fibonacci(n):
            global fibonacci_calls
            fibonacci_calls += 1
            if n in (0, 1):
                return n
            return fibonacci(n - 1) + fibonacci(n - 2)

        global fibonacci_calls

        filenames = [None, '/tmp/xxx%i' % random.randint(0, 9999999), '/tmp/xxx']

        for filename in filenames:

            print('filename', filename)

            fibonacci_calls = 0
            memo_object = gm.Memoize(filename=filename, verbose=True)
            memoized_fibonacci = memo_object(fibonacci)

            f6 = fibonacci(6)

            mf6 = memoized_fibonacci(6)
            memo_object.print_memoize_stats()
            if filename is None:
                print(memo_object.cache)

            calls0 = fibonacci_calls

            mf62 = memoized_fibonacci(6)
            memo_object.print_memoize_stats()
            if filename is None:
                print(memo_object.cache)

            assert f6 == mf6 and mf6 == mf62
            assert calls0 == fibonacci_calls, (calls0, fibonacci_calls)

if __name__ == '__main__':
    unittest.main()
