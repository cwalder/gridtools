from gridtools import cluster, qsub, helpers, paths
import itertools

_remote_submit_host = 'bracewell.hpc.csiro.au'

# local_host_tuple = [('short_circuit', None)]
# local_host_tuple = [('short_circuit', None), (True, None)]
# local_host_tuple = [(True, None)]
local_host_tuple = [('short_circuit', None), (True, None), (False, _remote_submit_host)]
# local_host_tuple = [(False, _remote_submit_host)]

qsub_kwargs = dict(mem=1.5 * 1024, maxtime='0:01:00')


def sync():
    for local, submit_host in local_host_tuple:
        paths.code_roots = ['~/workspace/gridtools/']
        if _remote_submit_host == submit_host:
            helpers.rsync_roots('code_roots', submit_host=submit_host)
            helpers.rsync_roots('sync_roots', submit_host=submit_host)


def test_cluster():

    for local, submit_host in local_host_tuple:

        tmpfn = '/tmp/jobs.pkl'

        aa1 = [([[2, 1, 4, 3, 0]], dict(reverse=True)), ([[-2, -1, -4, -3, -0]], dict(reverse=True)), ]
        ff1 = sorted

        aa2 = [([[2, 1, 4, 3, 0]], dict()), ([[-2, -1, -4, -3, -0]], dict()), ]
        ff2 = helpers.ftest_v2dict

        aa = [aa1, aa2]
        ff = [ff1, ff2]

        for a, f in zip(aa, ff):

            jobs = cluster.Jobs(local=local, submit_host=submit_host, qsub_kwargs=qsub_kwargs)

            for args, kwargs in a:
                jobs.add(f,  *args, **kwargs)

            jobs.qsub()
            helpers.dump(jobs, tmpfn)

            print('collecting...')
            results = jobs.collect(block=True, verbose=True)
            print('...collected')

            print('results', results)

            results2 = tuple(f(*args, **kwargs) for args, kwargs in a)
            assert results2 == results, (results2, results)

            del(jobs)
            jobs = helpers.load(tmpfn)
            results3 = jobs.collect()
            assert results3 == results, (results3, results)

            resultsdict = jobs.collect(as_dict=True)
            i, r = zip(*sorted(resultsdict.items(), key=lambda x: x[0]))
            assert r == results


def test_cluster_yield_partial():


    for local, submit_host in local_host_tuple:

        tmpfn = '/tmp/jobs.pkl'

        jobs = cluster.Jobs(local=local, submit_host=submit_host, qsub_kwargs=qsub_kwargs, yield_partial=True)
        jobs.add(itertools.chain, [0, 1], [2, 3])
        jobs.add(itertools.chain, [10, 20])
        jobs.qsub()
        helpers.dump(jobs, tmpfn)

        results = jobs.collect(block=True, verbose=True)

        assert results == (3, 20), results
        print('okay', local, submit_host)

        del(jobs)
        jobs = helpers.load(tmpfn)
        results2 = jobs.collect()

        assert results2 == results, (results2, results)

        resultsdict = jobs.collect(as_dict=True)
        i, r = zip(*sorted(resultsdict.items(), key=lambda x: x[0]))
        print(i, r)


if __name__ == '__main__':

    sync()
    # test_cluster_yield_partial()
    test_cluster()
